import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'firebase'

import { Header, Button, Spinner, CardSection } from './components/common';
import LoginForm from './components/LoginForm';


class App extends Component {
    state = { loggedIn: null };
    
    componentWillMount() {
        // fetch('http://localhost:3000').then((r)=>{
        //     console.log("got resulut",r.json())
        // }).catch((err)=>{
        //     console.log('err',err);
        // })
        firebase.initializeApp({
            apiKey: "AIzaSyCyuQzgCVkH_ibFTWxDsnjAwxXvb-IcbHM",
            authDomain: "react-test-auth-64f6c.firebaseapp.com",
            databaseURL: "https://react-test-auth-64f6c.firebaseio.com",
            projectId: "react-test-auth-64f6c",
            storageBucket: "react-test-auth-64f6c.appspot.com",
            messagingSenderId: "442649478534"
        });

        // listener of login and logout event
        firebase.auth().onAuthStateChanged((user) => {
            user ? this.setState({ loggedIn: true }) : this.setState({ loggedIn: false });
        });
    }

    renderContent() {
       
        switch (this.state.loggedIn) {
            case true:
                return (
                    <CardSection>
                        <Button onPressButton={() => firebase.auth().signOut()}>Log Out</Button>
                    </CardSection>
                );
            case false:
                return <LoginForm />;
            default:
                return 
                return (
                    <CardSection>
                        <Spinner size="large" />
                    </CardSection>
                );
        }
    }

    render() {
        return(
            <View>
                <Header headerText={{headerName: 'Authentication'}} />
                {this.renderContent()}
            </View>
        );
    }
}

export default App;
