import React from 'react';
import { Text, View } from 'react-native';

/* Creat a component */
const Header = (props) => {
    const { textStyle, viewStyle } = styles;

    return (

        // <View style={styles.viewStyle}> is valid without the definition of 
        // (const { textStyle, viewStyle } = styles;)
        <View style={viewStyle}>
            <Text style={textStyle}>{props.headerText.headerName}</Text>
        </View>

        // React-native 
        
    );
};
    

/* Styles for component */
const styles = {
    viewStyle: {
        backgroundColor: '#F8F8F8',
        // orthogonal positioning
        justifyContent: 'center',
        // horizontal positioning
        alignItems: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 30
    }
};

/* Make the component available to other parts of the app */
export { Header };

